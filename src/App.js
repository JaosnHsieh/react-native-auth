import React, { Component } from 'react';
import { View, Text } from 'react-native';
import firebase from 'firebase'

import { Header, Button, Spinner } from './components/common';
import LoginForm from './components/LoginForm'


class App extends Component {
    state = { loggedIn: null };

    componentWillMount() {
        firebase.initializeApp({
            apiKey: "AIzaSyCyuQzgCVkH_ibFTWxDsnjAwxXvb-IcbHM",
            authDomain: "react-test-auth-64f6c.firebaseapp.com",
            databaseURL: "https://react-test-auth-64f6c.firebaseio.com",
            projectId: "react-test-auth-64f6c",
            storageBucket: "react-test-auth-64f6c.appspot.com",
            messagingSenderId: "442649478534"
        });

        // listener of login and logout event
        firebase.auth().onAuthStateChanged((user) => {
            user ? this.setState({ loggedIn: true }) : this.setState({ loggedIn: false });
        });
    }

    renderContent() {
        switch (this.state.loggedIn) {
            case true:
                return (
                    <Button onPressButton={() => firebase.auth().signOut()}>Log Out</Button>
                );
            case false:
                return <LoginForm />;
            default:
                return <Spinner size="large" />;
        }
    }

    render() {
        return(
            <View>
                <Header headerText={{headerName: 'Authentication'}} />
                {this.renderContent()}
            </View>
        );
    }
}

export default App;
